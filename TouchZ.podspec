Pod::Spec.new do |s|
    s.name             = 'TouchZ'
    s.version          = '0.1.3'
    s.summary          = 'Beta testing framework.'
    s.homepage         = 'https://bitbucket.org/scand/touchez-ios'
    s.social_media_url = 'https://scand.com/'
    s.license          = { :type => 'GPLv3', :file => 'LICENSE' }
    s.author           = { 'Scand Ltd.' => 'info@scand.com' }
    s.source           = { :git => 'https://bitbucket.org/scand/touchez-ios.git', :tag => s.version.to_s }
    s.description      = <<-DESC
    Simple iOS library for capturing screenshots automatically while testing an app
    DESC

    s.ios.deployment_target = '8.0'
    s.frameworks = 'UIKit', 'Foundation'
    s.requires_arc     = true

    s.ios.vendored_frameworks = 'TouchZ/Frameworks/TouchZ.framework'
    s.preserve_paths = 'TouchZ/Frameworks/TouchZ.framework'

end