# TouchZ

[![CI Status](http://img.shields.io/travis/Pavel_Evsaev/TouchZ.svg?style=flat)](https://travis-ci.org/Pavel_Evsaev/TouchZ)
[![Version](https://img.shields.io/cocoapods/v/TouchZ.svg?style=flat)](http://cocoapods.org/pods/TouchZ)
[![License](https://img.shields.io/cocoapods/l/TouchZ.svg?style=flat)](http://cocoapods.org/pods/TouchZ)
[![Platform](https://img.shields.io/cocoapods/p/TouchZ.svg?style=flat)](http://cocoapods.org/pods/TouchZ)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

TouchZ is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "TouchZ"
```

## Author

Scand Ltd., info@scand.com

## License

TouchZ is available under the GPLv3 license. See the LICENSE file for more info.