//
//  main.m
//  TouchZ
//
//  Created by Pavel_Evsaev on 08/15/2016.
//  Copyright (c) 2016 Pavel_Evsaev. All rights reserved.
//

@import UIKit;
#import "TZAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, @"TZApplication", NSStringFromClass([TZAppDelegate class]));
    }
}
