//
//  TZAppDelegate.h
//  TouchZ
//
//  Created by Pavel_Evsaev on 08/15/2016.
//  Copyright (c) 2016 Pavel_Evsaev. All rights reserved.
//

@import UIKit;

@interface TZAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
